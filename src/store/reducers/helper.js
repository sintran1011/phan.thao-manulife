import { actionType } from "../actions/type";

const initialState = {
  keySearch: [],
  isLogin: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.SET_KEY_SEARCH: {
      state.keySearch = payload;
      return { ...state };
    }
    case actionType.SET_LOGIN: {
      state.isLogin = true;
      return { ...state };
    }

    default:
      return { ...state };
  }
};

export default reducer;
