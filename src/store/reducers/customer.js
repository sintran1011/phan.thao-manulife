import { actionType } from "../actions/type";

const initialState = {
  customer: [],
  customerTemp: {},
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    default:
      return { ...state };
  }
};

export default reducer;
