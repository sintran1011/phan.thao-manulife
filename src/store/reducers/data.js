import { actionType } from "../actions/type";

const initialState = {
  bankData: {
    bankInitialCapital: 0,
    bankInterest: 0,
    bankDepositPerYear: 0,
  },
  houseData: {
    houseInitialCapital: 0,
    houseInterest: 0,
  },
  indexData: {
    indexInterest: 0,
    indexDepositPerYear: 0,
  },
  targetMoney: 0,
  dataTableDepositMoney: [],
  ageCustomer: 0,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actionType.GET_DATA_TO_CALCULATE:
      state = { ...state, ...payload };
      return { ...state };

    case actionType.GET_TARGET_MONEY:
      state = { ...state, ...payload };
      return { ...state };

    case actionType.GET_DATA_TABLE_DEPOSIT_MONEY:
      state.dataTableDepositMoney = payload;
      return { ...state };

    default:
      return { ...state };
  }
};

export default reducer;
