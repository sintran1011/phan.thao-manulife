import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import customer from "./reducers/customer";
import data from "./reducers/data";
import helper from "./reducers/helper";

const rootReducer = combineReducers({
  customer,
  data,
  helper,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export default store;
