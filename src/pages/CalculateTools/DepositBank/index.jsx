import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { actionType } from "../../../store/actions/type";
// import { formatMoney } from "../formatMoney";
import style from "./style.module.css";

const DepositBank = () => {
  const columns = [
    {
      title: "Năm hợp đồng",
      dataIndex: "yearContract",
      key: "yearContract",
    },
    {
      title: "Tuổi khách hàng",
      dataIndex: "ageCustomer",
      key: "ageCustomer",
    },

    {
      title: "Năm",
      dataIndex: "year",
      key: "year",
    },
    {
      title: "Tiền gửi định kỳ",
      dataIndex: "bankDepositPerYear",
      key: "bankDepositPerYear",
    },
    {
      title: "Số dư đầu kỳ",
      dataIndex: "openingBalance",
      key: "openingBalance",
    },
    {
      title: "Số dư cuối kỳ",
      dataIndex: "closingBalance",
      key: "closingBalance",
    },
  ];

  const {
    bankInitialCapital = 0,
    bankInterest = 0,
    bankDepositPerYear = 0,
  } = useSelector((state) => state.data.bankData);
  const {
    targetMoney = 0,
    dataTableDepositMoney = [],
    ageCustomer = 0,
  } = useSelector((state) => state.data);
  const { keySearch = [] } = useSelector((state) => state.helper);

  const dispatch = useDispatch();

  const getDate = new Date();
  const getYear = getDate.getFullYear();

  const calculate = () => {
    let data = [];
    let closingBalance = 0;
    let openingBalance = bankDepositPerYear + bankInitialCapital;
    for (let i = 1; i < 100; i++) {
      closingBalance = openingBalance * (1 + bankInterest / 100);
      data.push({
        key: i,
        yearContract: i,
        year: getYear + (i - 1),
        bankDepositPerYear: addCommas(bankDepositPerYear),
        openingBalance: addCommas(openingBalance),
        closingBalance: addCommas(closingBalance),
        ageCustomer: ageCustomer + i,
      });
      openingBalance = closingBalance + bankDepositPerYear;
    }
    dispatch({
      type: actionType.GET_DATA_TABLE_DEPOSIT_MONEY,
      payload: data,
    });
  };

  // filter by searchKey
  const filterData = () => {
    if (keySearch[0] === 0) {
      return dataTableDepositMoney;
    }
    if (keySearch[0] !== 0) {
      const dataFilter = dataTableDepositMoney.filter((item) =>
        keySearch.includes(item.key)
      );
      return dataFilter;
    }
  };

  useEffect(() => {
    calculate();
  }, [bankDepositPerYear, bankInitialCapital, bankInterest]);

  useEffect(() => {
    filterData();
  }, [keySearch]);

  const removeCommas = (number) => {
    while (number.search(",") >= 0) {
      number = (number + "").replace(",", "");
    }

    return Number(number);
  };

  console.log(removeCommas("100,000,000"));

  const addCommas = (number) => {
    return number.toFixed(0).replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");
  };

  const handleChangeColor = (data, index) => {
    if (targetMoney === 0 && index % 2 === 0) {
      return "light";
    } else if (targetMoney === 0 && index % 2 !== 0) {
      return "dark";
    }

    if (removeCommas(data.closingBalance) > targetMoney) {
      return "red";
    } else if (removeCommas(data.closingBalance) < targetMoney) {
      return "green";
    }
  };

  const pagination = {
    showTotal: (total, range) =>
      `Năm ${range[0]}-${range[1]} trong kỳ hạn ${total} năm`,
    total: 99,
    position: ["bottomCenter"],
    showSizeChanger: true,
    pageSizeOptions: ["10", "20", "50", "100"],
  };

  const renderTable = () => {
    if (keySearch[0] === 0 || keySearch.length === 0) {
      return dataTableDepositMoney;
    }
    if (keySearch.length > 0) {
      return filterData();
    }
  };

  return (
    <div>
      <Table
        pagination={pagination}
        columns={columns}
        dataSource={renderTable()}
        scroll={{ x: "fit-content", y: 350 }}
        style={{ textAlign: "center" }}
        bordered
        rowClassName={(data, index) => handleChangeColor(data, index)}
      />
    </div>
  );
};

export default DepositBank;
