import React, { useState } from "react";
import style from "./style.module.css";
import { Form, InputNumber } from "antd";
import { useDispatch } from "react-redux";
import { actionType } from "../../../store/actions/type";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import Button from "../../../components/Button";

const FormInfo = () => {
  const [form] = Form.useForm();
  const [money, setMoney] = useState(0);
  const dispatch = useDispatch();
  const handleSubmit = () => {
    const values = form.getFieldsValue();
    
    dispatch({
      type: actionType.GET_DATA_TO_CALCULATE,
      payload: {
        bankData: {
          bankInitialCapital: values.bankInitialCapital,
          bankInterest: values.bankInterest,
          bankDepositPerYear: values.bankDepositPerYear,
        },
        houseData: {
          houseInitialCapital: values.houseInitialCapital,
          houseInterest: values.houseInterest,
        },
        indexData: {
          indexInterest: values.indexInterest,
          indexDepositPerYear: values.indexDepositPerYear,
        },
        ageCustomer: values.ageCustomer,
      },
    });
    alert();
  };

  const handleChange = (val) => {
    setMoney(val);
    dispatch({
      type: actionType.GET_TARGET_MONEY,
      payload: { targetMoney: val },
    });
  };

  const navigate = useNavigate();

  const alert = async () => {
    const {
      bankDepositPerYear,
      bankInitialCapital,
      bankInterest,
      houseInitialCapital,
      houseInterest,
      indexDepositPerYear,
      indexInterest,
      ageCustomer,
    } = form.getFieldsValue();
    await Swal.fire({
      title: "Bạn muốn tính gì?",
      input: "radio",
      icon: "question",
      confirmButtonText: "Xác nhận",
      inputOptions: {
        BenFranklin: "Ben Franklin",
        DepositBank: "Tiền gửi ngân hàng",
      },
      inputValidator: (value) => {
        if (!value) {
          return "Không muốn tính gì nhập liệu chi vậy :v";
        }
        if (value === "BenFranklin") {
          if (money === 0) {
            return "Không nhập số tiền mơ ước lấy gì tính má";
          }
          if (bankDepositPerYear === undefined) {
            return "Nhập tiền tiết kiệm / năm của ngân hàng dùm ";
          }
          if (bankInterest === undefined) {
            return "Nhập lãi suất ngân hàng dùm ";
          }
          if (houseInitialCapital === undefined) {
            return "Nhập tiền vốn ban đầu BDS / năm dùm ";
          }
          if (houseInterest === undefined) {
            return "Nhập tỉ suất lợi nhuận BĐS dùm ";
          }
          if (indexInterest === undefined) {
            return "Nhập tỉ suất lợi nhuận chứng khoán dùm";
          }
          if (indexDepositPerYear === undefined) {
            return "Nhập tiền tiết kiệm / năm của chứng khoán dùm";
          }
        }
        if (value === "DepositBank") {
          if (bankInitialCapital === undefined) {
            return "Nhập tiền gửi ban đầu của sổ tk dùm ";
          }
          if (bankInterest === undefined) {
            return "Nhập lãi suất ngân hàng dùm ";
          }
          if (bankDepositPerYear === undefined) {
            return "Nhập tiền tiết kiệm / năm của ngân hàng dùm ";
          }
          if (ageCustomer === undefined) {
            return "Nhập tuổi khách hàng dùm";
          }
        }
      },
    }).then((res) => {
      const { isConfirmed, value } = res;

      if (isConfirmed) {
        navigate(`/${value}`);
      }
    });
  };

  return (
    <div className={style.container}>
      <div>
        <InputNumber
          onChange={(val) => handleChange(val)}
          style={{
            height: "30px",
            width: "250px",
            marginLeft: "20px",
          }}
          placeholder="Your dream target money!"
          formatter={(value) =>
            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          }
          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
          step="100000000"
        />
      </div>
      <h1 className="text-center font-semibold text-3xl py-5">
        Vui lòng nhập số liệu
      </h1>
      <Form layout="inline" size={"large"} form={form}>
        <div className={style.formContent}>
          <h1
            style={{ fontWeigth: 700, fontSize: "30px", textAlign: "center" }}
          >
            Kênh ngân hàng
          </h1>
          <Form.Item name="bankInitialCapital" label="Tiền gửi ban đầu">
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="100000000"
            />
          </Form.Item>
          <Form.Item name="bankInterest" label="Lãi suất ngân hàng">
            <InputNumber
              formatter={(value) => `${value}%`}
              parser={(value) => value.replace("%", "")}
              max="50"
            />
          </Form.Item>

          <Form.Item name="bankDepositPerYear" label="Tiền tiết kiệm / năm">
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="10000000"
            />
          </Form.Item>
          <Form.Item name="ageCustomer" label="Tuổi khách hàng">
            <InputNumber />
          </Form.Item>
        </div>
        <div className={style.formContent}>
          <h1
            style={{ fontWeigth: 700, fontSize: "30px", textAlign: "center" }}
          >
            Kênh BĐS
          </h1>
          <Form.Item
            name="houseInitialCapital"
            style={{ color: "whitesmoke" }}
            label="Tiền vốn ban đầu"
          >
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="100000000"
            />
          </Form.Item>
          <Form.Item name="houseInterest" label="Tỉ suất lợi nhuận">
            <InputNumber
              formatter={(value) => `${value}%`}
              parser={(value) => value.replace("%", "")}
              max="50"
            />
          </Form.Item>

          {/* <Form.Item name="houseDepositPerYear" label="Tiền tiết kiệm / năm">
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="10000000"
            />
          </Form.Item> */}
        </div>
        <div className={style.formContent}>
          <h1
            style={{ fontWeigth: 700, fontSize: "30px", textAlign: "center" }}
          >
            Kênh chứng khoán
          </h1>
          {/* <Form.Item
            name="indexInitialCapital"
            style={{ color: "whitesmoke" }}
            label="Tiền vốn ban đầu"
          >
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="100000000"
            />
          </Form.Item> */}
          <Form.Item name="indexInterest" label="Tỉ suất lợi nhuận">
            <InputNumber
              formatter={(value) => `${value}%`}
              parser={(value) => value.replace("%", "")}
              max="50"
            />
          </Form.Item>

          <Form.Item name="indexDepositPerYear" label="Tái đầu tư mỗi năm">
            <InputNumber
              formatter={(value) =>
                `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
              }
              parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
              step="10000000"
            />
          </Form.Item>
        </div>
      </Form>
      <div
        onClick={handleSubmit}
        style={{ width: "13%", margin: "30px auto", cursor: "pointer" }}
      >
        <Button content="GET GO!!!" />
      </div>
    </div>
  );
};

export default FormInfo;
