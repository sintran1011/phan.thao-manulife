import React, { useEffect, useState } from "react";
import style from "./style.module.css";
import bgleft from "../../../assets/Bank-left.png";
import bgright from "../../../assets/Bank-right.png";
import { useSelector } from "react-redux";

const BenFranklin = () => {
  const {
    targetMoney = 0,
    bankData: {
      bankInitialCapital = 0,
      bankInterest = 0,
      bankDepositPerYear = 0,
    } = {},
    houseData: {
      houseInitialCapital = 0,
      houseInterest = 0,
      houseDepositPerYear = 0,
    } = {},
    indexData: {
      indexInitialCapital = 0,
      indexInterest = 0,
      indexDepositPerYear = 0,
    } = {},
  } = useSelector((state) => state.data);

  const [bankTime, setBankTime] = useState(0);
  const [houseTime, setHouseTime] = useState(0);
  const [indexTime, setIndexTime] = useState(0);

  const formatNumToBillion = (num) => {
    return (num / 1000000000).toFixed(1);
  };

  const calculateBank = () => {
    let totalBank = 0;
    for (let i = 1; i < 50; i++) {
      totalBank = (totalBank + bankDepositPerYear) * (1 + bankInterest / 100);
      if (totalBank >= targetMoney) {
        setBankTime(i);
        return;
      }
    }
  };
  const calculateHouse = () => {
    let totalHouse = 0;
    for (let i = 0; i < 50; i++) {
      totalHouse = houseInitialCapital * (1 + houseInterest / 100) ** i;
      if (totalHouse >= targetMoney) {
        setHouseTime(i);
        return;
      }
    }
  };

  const calculateIndex = () => {
    let totalIndex = 0;
    for (let i = 1; i < 50; i++) {
      totalIndex =
        (totalIndex + indexDepositPerYear) * (1 + indexInterest / 100);
      if (totalIndex >= targetMoney) {
        setIndexTime(i);
        console.log(totalIndex);
        return;
      }
    }
  };

  // Làm tròn tới đơn vị ngàn sau đó thêm dấu .
  const formatByThousand = (number) => {
    return ((number / 1000).toFixed(0) * 1000).toLocaleString();
  };

  useEffect(() => {
    calculateBank();
    calculateHouse();
    calculateIndex();
  });

  return (
    <div className={style.container}>
      <div className={style.image}>
        <div className={style.overlay}></div>
        <img className={style.bgleft} src={bgleft} alt="bgleft" />
        <img className={style.bgright} src={bgright} alt="bgright" />
        <div className={style.text}>
          <div className={style.text_header}>The Ben Franklin</div>

          <div className={style.text_body}>
            <div className={style.title}>
              Mua tài sản đảm bảo không lãi thanh toán dần ?
            </div>
            <div className={style.text_right}>
              <p>{targetMoney.toLocaleString()} VND</p>
            </div>
          </div>
          <div className={style.content}>
            <div className={style.content_left}>
              <div style={{ margin: "0" }} className={style.text_right}>
                {targetMoney.toLocaleString()} VND
              </div>
              <div
                style={{ margin: "20px 0 10px 0" }}
                className={style.content2}
              >
                <p style={{ margin: 0 }}>Vốn</p>
                <p style={{ margin: 0 }}>
                  {formatNumToBillion(bankDepositPerYear * 20)} tỷ
                </p>
              </div>
              <div
                style={{ fontSize: "20px", fontWeight: 700, color: "#ffffff" }}
              >
                20 năm
              </div>
              <div
                style={{ fontSize: "20px", fontWeight: 700, color: "#ffffff" }}
              >
                10%
              </div>
              <div
                style={{ fontSize: "25px", fontWeight: 700, color: "yellow" }}
              >
                {formatByThousand(bankDepositPerYear / 365)}
                VND/Ngày
              </div>
              <p
                style={{
                  color: "white",
                  fontSize: "10px",
                  paddingTop: "165px",
                }}
              >
                Nguồn: Manulife và các tạp chí điện tử:
                Tinnhanhchungkhoan,cafeF,Vietnambiz,Vn.tradingnew.com
              </p>
            </div>
            <div className={style.content_right}>
              <div className={style.content_mini}>
                <div className={style.round}>Ngân hàng</div>
                <div
                  style={{
                    width: "350px",
                    color: "#ffffff",
                    fontWeight: 700,
                    fontSize: "25px",
                    marginLeft: "70px",
                  }}
                >
                  <span>{bankInterest.toLocaleString()}%</span>
                  <span>{bankTime}</span>
                  <span>
                    {formatNumToBillion(bankTime * bankDepositPerYear)} tỷ
                  </span>
                  <p style={{ margin: 0 }}>
                    {bankDepositPerYear.toLocaleString()}VND/Năm
                  </p>
                </div>
              </div>
              <div style={{ padding: "20px 0" }} className={style.content_mini}>
                <div className={style.round}>BĐS</div>
                <div
                  style={{
                    width: "350px",
                    color: "#ffffff",
                    fontWeight: 700,
                    fontSize: "25px",
                    marginLeft: "110px",
                  }}
                >
                  <span>{houseInterest.toLocaleString()}%</span>
                  <span>{houseTime}</span>
                  <span>{formatNumToBillion(houseInitialCapital)} tỷ</span>
                  <p style={{ margin: 0 }}>
                    {houseInitialCapital.toLocaleString()} VND
                  </p>
                </div>
              </div>
              <div className={style.content_mini}>
                <div className={style.round}>VN Index</div>
                <div
                  style={{
                    width: "350px",
                    color: "#ffffff",
                    fontWeight: 700,
                    fontSize: "25px",
                    marginLeft: "150px",
                  }}
                >
                  <span>{indexInterest.toLocaleString()}%</span>
                  <span>{indexTime}</span>
                  <span>
                    {formatNumToBillion(indexTime * indexDepositPerYear)} tỷ
                  </span>
                  <p style={{ margin: 0 }}>
                    {indexDepositPerYear.toLocaleString()} VND/Năm
                  </p>
                </div>
              </div>
              <p
                style={{
                  margin: 0,
                  color: " #ffffff",
                  fontSize: "30px",
                  fontWeight: 700,
                  lineHeight: "30px",
                  textAlign: "right",
                  padding: "60px 0",
                }}
              >
                Đầu tư dần để có tài sản ?
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BenFranklin;
