import {
  Button,
  Cascader,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Select,
  Switch,
  TreeSelect,
} from "antd";

import style from "./style.module.css";

const CustomerForm = () => {
  const [form] = Form.useForm();

  const handleSubmit = () => {
    const values = form.getFieldValue();
    console.log(values);
  };

  return (
    <div className={style.container}>
      <Form
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 14,
        }}
        layout="horizontal"
        size={"large"}
        form={form}
        onSubmitCapture={handleSubmit}
      >
        <h1 style={{ fontSize: "30px", textAlign: "center" }}>
          Thông tin khách hàng
        </h1>
        <Form.Item name="id" label="Mã khách hàng">
          <Input />
        </Form.Item>
        <Form.Item name="name" label="Tên khách hàng">
          <Input />
        </Form.Item>
        <Form.Item label="Select">
          <Select>
            <Select.Option value="demo">Demo</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item label="TreeSelect">
          <TreeSelect
            treeData={[
              {
                title: "Light",
                value: "light",
                children: [
                  {
                    title: "Bamboo",
                    value: "bamboo",
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="Cascader">
          <Cascader
            options={[
              {
                value: "zhejiang",
                label: "Zhejiang",
                children: [
                  {
                    value: "hangzhou",
                    label: "Hangzhou",
                  },
                ],
              },
            ]}
          />
        </Form.Item>
        <Form.Item label="DatePicker">
          <DatePicker />
        </Form.Item>
        <Form.Item label="InputNumber">
          <InputNumber />
        </Form.Item>
        <Form.Item label="Switch" valuePropName="checked">
          <Switch />
        </Form.Item>
        <Form.Item label="Button">
          <Button htmlType="submit" type="primary">
            Button
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CustomerForm;
