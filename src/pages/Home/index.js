import React from "react";
import { Carousel } from "antd";
import style from "./style.module.css";
import banner1 from "../../assets/banner1.jpg";
import banner2 from "../../assets/banner2.PNG";
import banner3 from "../../assets/banner3.jpg";
import banner4 from "../../assets/banner4.jpg";

const Home = () => {
  return (
    <Carousel style={{ width: "70%", margin: "auto" }} autoplay={false}>
      <div className={style.contentStyle}>
        <img src={banner1} alt="banner1" />
      </div>
      <div className={style.contentStyle}>
        <img src={banner2} alt="banner2" />
      </div>
      <div className={style.contentStyle}>
        <img src={banner3} alt="banner3" />
      </div>
      <div className={style.contentStyle}>
        <img src={banner4} alt="banner4" />
      </div>
    </Carousel>
  );
};

export default Home;
