import { Form } from "antd";
import React from "react";
import Swal from "sweetalert2";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import LogoLogin from "../../assets/logoLogin.png";
import { actionType } from "../../store/actions/type";
import "./style.css";

const Login = () => {
  const [form] = Form.useForm();
  const { isLogin } = useSelector((state) => state.helper);

  const login = {
    userName: "thanhthao",
    passWord: "10112018",
  };

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSubmit = () => {
    const values = form.getFieldsValue();
    const { username, password } = values;
    if (username === login.userName && password === login.passWord) {
      dispatch({
        type: actionType.SET_LOGIN,
      });
    }
    if (isLogin) {
      navigate("/");
    }
  };

  return (
    <section className="h-full gradient-form bg-gray-200">
      <div className="container h-full">
        <div className="flex justify-center items-center h-full text-gray-800">
          <div className="xl:w-10/12">
            <div className="bg-white shadow-lg rounded-lg">
              <div className="lg:flex lg:flex-wrap g-0">
                <div className="lg:w-6/12 px-4 md:px-0">
                  <div className="md:p-12 md:mx-6">
                    <div className="text-center">
                      <img
                        className="mx-auto w-64"
                        src={LogoLogin}
                        alt="logo"
                      />
                      <h4 className="text-xl font-semibold mt-1 mb-12 pb-1">
                        Chúng tôi là Madza Team
                      </h4>
                    </div>
                    <Form layout="horizontal" size={"large"} form={form}>
                      <p className="mb-4">Vui lòng nhập thông tin tài khoản</p>
                      <div className="mb-4 login">
                        <Form.Item name="username">
                          <input
                            type="text"
                            className=" w-full px-3 py-1.5 text-base font-normal text-gray-700  border border-solid border-gray-300 rounded  focus:border-green-600 focus:border-2 focus:outline-none"
                            placeholder="Tên đăng nhập"
                          />
                        </Form.Item>
                      </div>
                      <div className="mb-4 login">
                        <Form.Item name="password">
                          <input
                            type="password"
                            className=" w-full px-3 py-1.5 text-base font-normal text-gray-700  border border-solid border-gray-300 rounded  focus:border-green-600 focus:border-2 focus:outline-none"
                            placeholder="Mật khẩu"
                          />
                        </Form.Item>
                      </div>
                      <div className="text-center pt-1 mb-12 pb-1">
                        <button
                          onClick={handleSubmit}
                          className="custom-btn btn-5 px-6 py-2.5 text-white font-bold text-sm  rounded shadow-md  focus:outline-none w-full mb-3"
                        >
                          <span>Đăng nhập</span>
                        </button>
                      </div>
                    </Form>
                  </div>
                </div>
                <div className="lg:w-6/12 flex items-center lg:rounded-r-lg rounded-b-lg lg:rounded-bl-none bg-gradient-to-br from-gray-200 via-green-500 to-cyan-900">
                  <div className="text-white px-4 py-6 md:p-12 md:mx-6">
                    <h4 className="text-2xl font-bold mb-6 text-white">
                      Chúng tôi ko đơn giản là bán bảo hiểm, mà còn là bán sự
                      bảo vệ
                    </h4>
                    <p className="text-sm">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                      ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Login;
