import React from "react";
import style from "./style.module.css";

const Button = (props) => {
  const { content } = props;
  return <div className={style.btn}>{content}</div>;
};

export default Button;
