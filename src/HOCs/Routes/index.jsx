import { Navigate } from "react-router-dom";

export const AuthRoute = ({ children, navigatePath, condition }) => {
  if (condition) {
    return children;
  }
  return <Navigate to="/Login" />;
};
