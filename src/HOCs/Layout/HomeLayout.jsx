import { Input, Layout, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  HomeOutlined,
  ToolOutlined,
  CodeSandboxOutlined,
  CloudDownloadOutlined,
  AliwangwangOutlined,
  MailOutlined,
} from "@ant-design/icons";
import { useState } from "react";
import style from "./style.module.css";
import logo from "../../assets/manulife.jpg";
import iconMini from "../../assets/iconMini.png";
import { Outlet, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { actionType } from "../../store/actions/type";

const { Header, Sider, Content } = Layout;
const { Search } = Input;

const HomeLayout = () => {
  const [collapsed, setCollapsed] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const menuItem = [
    {
      key: "1",
      icon: <UserOutlined height={20} width={20} />,
      label: "Khách hàng",
      children: [
        {
          key: "CustomerForm",
          label: "Form thông tin",
          // onClick: (key) => navigate(key.key),
        },
        {
          key: "CustomerInfo",
          label: "Danh sách KH",
          // onClick: (key) => navigate(key.key),
        },
      ],
    },
    {
      key: "2",
      icon: <CodeSandboxOutlined />,
      label: "Sản phẩm",
      children: [
        {
          key: "FuturePresent",
          label: "Món quà tương lai",
          // onClick: (key) => navigate(key.key),
        },
        {
          key: "2b",
          label: "Bảo hiểm nhân thọ",
        },
      ],
    },
    {
      key: "3",
      icon: <ToolOutlined />,
      label: "Bảng tính ",
      children: [
        {
          key: "FormInfo",
          label: "Form nhập dữ liệu",
          onClick: (key) => navigate(key.key),
        },
        {
          key: "BenFranklin",
          label: "Ben Franklin",
          onClick: (key) => navigate(key.key),
        },
        {
          key: "DepositBank",
          label: "Tiền gửi tiết kiệm",
          onClick: (key) => navigate(key.key),
        },
      ],
    },
    {
      key: "4",
      icon: <CloudDownloadOutlined />,
      label: "Dịch vụ",
      children: [
        {
          key: "3a",
          label: "vd1",
        },
        {
          key: "3b",
          label: "vd2",
        },
      ],
    },
    {
      key: "5",
      icon: <AliwangwangOutlined />,
      label: "Về chúng tôi",
    },
    {
      key: "6",
      icon: <MailOutlined />,
      label: "Liên hệ",
    },
    {
      key: "/",
      icon: <HomeOutlined />,
      label: "Trang chủ",
      onClick: () => navigate("/"),
    },
  ];

  const logoHeader = () => {
    if (collapsed === false) {
      return (
        <div className={style.logo}>
          <img width="50%" src={logo} alt="logo" />
          <p>Phan.Thao</p>
        </div>
      );
    } else {
      return (
        <div className={style.logoMini}>
          <img width="100%" height="100%" src={iconMini} alt="logo" />
        </div>
      );
    }
  };

  const onSearch = (values) => {
    const valueTemp = values.target.value.split(",");
    let valueArray = valueTemp.map((item) => Number(item));

    dispatch({
      type: actionType.SET_KEY_SEARCH,
      payload: valueArray,
    });
  };

  return (
    <Layout>
      <Sider theme="dark" trigger={null} collapsible collapsed={collapsed}>
        {logoHeader()}
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={menuItem}
        />
      </Sider>
      <Layout className={style["site-layout"]}>
        <Header
          className={style["site-layout-background"]}
          style={{ padding: "20px 0", display: "flex" }}
        >
          {collapsed ? (
            <MenuUnfoldOutlined onClick={() => setCollapsed(!collapsed)} />
          ) : (
            <MenuFoldOutlined onClick={() => setCollapsed(!collapsed)} />
          )}
          <div style={{ width: "50%", margin: "auto" }}>
            <Search
              placeholder="What are you looking for? "
              onChange={onSearch}
            />
          </div>
        </Header>
        <Content
          className={style["site-layout-background"]}
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            position: "relative",
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default HomeLayout;
