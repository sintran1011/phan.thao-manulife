import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomeLayout from "./HOCs/Layout/HomeLayout";
import Home from "./pages/Home";
// import CustomerForm from "./pages/CustomerForm";
// import CustomerInfo from "./pages/CustomerInfo";
import BenFranklin from "./pages/CalculateTools/BenFranklin";
import FuturePresent from "./pages/Product/FuturePresent";
import FormInfo from "./pages/CalculateTools/FormInfo";
import DepositBank from "./pages/CalculateTools/DepositBank";
import Login from "./pages/Login";
import { useSelector } from "react-redux";
import { AuthRoute } from "./HOCs/Routes";

function App() {
  const { isLogin = false } = useSelector((state) => state.helper);
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomeLayout />}>
          <Route
            index
            element={
              <AuthRoute condition={isLogin} navigativePath="/Login">
                <Home />
              </AuthRoute>
            }
          />

          {/* <Route
            path="CustomerForm"
            element={<AuthRoute condition={isLogin} navigativePath="/" />}
          >
            <Route element={<CustomerForm />} />
          </Route> */}
          {/* <Route path="CustomerInfo" element={<CustomerInfo />} /> */}
          <Route
            path="FormInfo"
            element={
              <AuthRoute condition={isLogin} navigativePath="/Login">
                <FormInfo />
              </AuthRoute>
            }
          />
          <Route
            path="BenFranklin"
            element={
              <AuthRoute condition={isLogin} navigativePath="/Login">
                <BenFranklin />
              </AuthRoute>
            }
          />
          <Route
            path="FuturePresent"
            element={
              <AuthRoute condition={isLogin} navigativePath="/Login">
                <FuturePresent />
              </AuthRoute>
            }
          />
          <Route
            path="DepositBank"
            element={
              <AuthRoute condition={isLogin} navigativePath="/Login">
                <DepositBank />
              </AuthRoute>
            }
          />

          <Route path="Login" element={<Login />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
